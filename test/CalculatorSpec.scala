import java.math.{MathContext, RoundingMode}
import java.nio.charset.StandardCharsets
import java.util.Base64

import controllers.CalculatorController
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.Status
import play.api.libs.json.{Reads, _}
import play.api.mvc.Result
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.Future

class CalculatorSpec extends PlaySpec  with GuiceOneAppPerSuite {

  // ----- //
  // TESTS //
  // ----- //
  "CalculatorController" should {

    "send BAD_REQUEST on when no query" in {
      val result = route(app, FakeRequest(GET, "/calculus")).get
      status(result) mustBe Status.BAD_REQUEST
    }

    "send BAD_REQUEST when invalid base64 utf-8" in {
      val result = route(app, FakeRequest(GET, "/calculus?query=asd-asd")).get
      status(result) mustBe Status.BAD_REQUEST
      assertErrorJson(result, CalculatorController.couldNotDecodeMessage)
    }

    "send OK with ERROR when empty query" in {
      val result = route(app, FakeRequest(GET, "/calculus?query=")).get
      status(result) mustBe Status.OK
      assertErrorJson(result, CalculatorController.emptyInputMessage)
    }

    // "1/0 divide by zero"
    "send OK with ERROR - 1/0 divide by zero " in {
      val nonEncodedQuery = "1/0"
      val encodedQuery = encodeToBase64UTF8(nonEncodedQuery)
      val result = route(app, FakeRequest(GET, "/calculus?query=" + encodedQuery)).get
      status(result) mustBe Status.OK
      assertErrorJson(result, "Division by zero")
    }

    "send OK with ERROR - Extra ( " in {
      val nonEncodedQuery = "(2 * (23/(3*3))- 23 * (2*3)"
      val encodedQuery = encodeToBase64UTF8(nonEncodedQuery)
      val result = route(app, FakeRequest(GET, "/calculus?query=" + encodedQuery)).get
      status(result) mustBe Status.OK
      assertErrorJson(
        result,
        CalculatorController.mismatchedParenthesesMessage)
    }


    "send OK with ERROR - Illegal character \".\" " in {
      val nonEncodedQuery = "2 * (23/(3*3))- 23.0 * (2*3)"
      val encodedQuery = encodeToBase64UTF8(nonEncodedQuery)
      val result = route(app, FakeRequest(GET, "/calculus?query=" + encodedQuery)).get
      status(result) mustBe Status.OK
      assertErrorJson(
        result,
        CalculatorController.illegalCharacterMessageBase + ".")
    }

    "send OK with ERROR - (--1)" in {
      assertWholeRequest_EvalFail(
        "(--1)", CalculatorController.generalEvaluationErrorMessage)
    }

    "send OK with ERROR - ()" in {
      assertWholeRequest_EvalFail(
        "()", CalculatorController.generalEvaluationErrorMessage)
    }

    "send OK with ERROR - \"-\"" in {
      assertWholeRequest_EvalFail(
        "-", CalculatorController.generalEvaluationErrorMessage)
    }

    // Valid queries
    val nonEncodedQuery1 = "2 * (23/(3*3))- 23 * (2*3)"
    "send OK - with non encoded query=\"" + nonEncodedQuery1 + "\"" in {
      val expectedResult1 = BigDecimal(
        2.0 * (23.0 / (3.0 * 3.0)) - 23.0 * (2.0 * 3.0), getMathContext()
      )
      val encodedQuery = encodeToBase64UTF8(nonEncodedQuery1)
      encodedQuery mustEqual "MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk="
      expectedResult1 mustEqual BigDecimal("-132.888888889")
      assertWholeRequest_Success(nonEncodedQuery1, expectedResult1)
    }

    val nonEncodedQuery2 = "2     *(23/(3*3))- ((23)) * (2  *3)  "
    "send OK - with non encoded query=\"" + nonEncodedQuery2 + "\"" in {
      val expectedResult2 = BigDecimal(
        2.0 * (23.0 / (3.0 * 3.0)) - 23.0 * (2.0 * 3.0), getMathContext()
      )
      assertWholeRequest_Success(nonEncodedQuery2, expectedResult2)
    }

    "send OK - with non encoded query=\"-1\"" in {
      assertWholeRequest_Success("-1", -1)
    }

    "send OK - with non encoded query=\"0-(1)\"" in {
      assertWholeRequest_Success("0-(1)", -1)
    }

    "send OK - with non encoded query=\"-(-1)\"" in {
      assertWholeRequest_Success("-(-1)", 1)
    }

    "send OK - with non encoded query=\"(-1)\"" in {
      assertWholeRequest_Success("(-1)", -1)
    }

    "send OK - with non encoded query=\"(((1)   ) )\"" in {
      assertWholeRequest_Success("( ((1)   ) )", 1)
    }

    "send OK - with non encoded query=\"-(1)\"" in {
      // This required hax or touching to Expression code
      // added 0 to front
      assertWholeRequest_Success("-(1)", -1)
    }


  } // END OF TESTS


  // ------- //
  // HELPERS //
  // ------- //
  /**
    * @return mathContext with same precision as CalculatorController
    */
  def getMathContext(): MathContext = {
    new MathContext(CalculatorController.precision, RoundingMode.HALF_UP)
  }


  def encodeToBase64UTF8(s: String): String = {
    /*println("NON ENCODED: " + s)
    println(
      "https://fast-forest-49909.herokuapp.com/calculus?query=" +
      Base64.getEncoder.encodeToString(s.getBytes(StandardCharsets.UTF_8))
    )*/
    Base64.getEncoder.encodeToString(s.getBytes(StandardCharsets.UTF_8))
  }

  /** 1) Result has json contentType
    * 2) parses json from content (does not test)
    * @param result
    * @return
    */
  def assertJson(result: Future[Result]): JsValue =  {
    contentType(result) mustBe Some("application/json")
    Json.parse(contentAsString(result))
  }

  /**
    * Test <fieldName> field
    * 1) has "error" field
    * 2) has expectedVal
    * @param parsedJsonContent
    * @param expectedVal
    */
  def assertJsonField[T: Reads](fieldName: String,
                                parsedJsonContent: JsValue, expectedVal: T): Unit = {
    // 1) has "<fieldName>" field
    val fieldIsDefined = parsedJsonContent \ fieldName
    assert(fieldIsDefined.isDefined)
    val fieldIsT = fieldIsDefined.get.validate[T]
    assert(fieldIsT.isSuccess)
    // 2) has expectedVal
    val fieldVal = fieldIsT.get
    fieldVal mustEqual expectedVal
  }

  /** Test "error" field
    * 1) has "error" field
    * 2) has expectedVal
    * @param parsedJsonContent
    * @param expectedVal
    */
  def assertJsonField_error(parsedJsonContent: JsValue, expectedVal: Boolean): Unit = {
    assertJsonField("error", parsedJsonContent, expectedVal)
  }

  /** Test "message" field
    * 1) has "message" field
    * 2) has expectedVal
    * @param parsedJsonContent
    * @param expectedVal
    */
  def assertJsonField_message(parsedJsonContent: JsValue, expectedVal: String): Unit = {
    assertJsonField("message", parsedJsonContent, expectedVal)
  }

  /** Test "result" field
    * 1) has "result" field
    * 2) has expectedVal
    * @param parsedJsonContent
    * @param expectedVal
    */
  def assertJsonField_result(parsedJsonContent: JsValue, expectedVal: BigDecimal): Unit = {
    assertJsonField("result", parsedJsonContent, expectedVal)
  }

  /** 1) has error field and it has value true
    * 2) has message field with exceptedMessage (Optional)
    * @param result
    * @param expectedMessage
    */
  def assertErrorJson(result: Future[Result], expectedMessage: String = null): Unit ={
    val parsedJsonContent: JsValue = assertJson(result)
    assertJsonField_error(parsedJsonContent, true)
    expectedMessage match {
      case null =>
      case expectedMessage: String => assertJsonField_message(parsedJsonContent, expectedMessage)
    }
  }

  /** Assert whole request
    * query is given and it is validly encoded, but the evaluation
    * of the query expression is expected to fail.
    * @param nonEncodedQuery
    * @param expectedMessage
    */
  def assertWholeRequest_EvalFail(nonEncodedQuery: String,
                                  expectedMessage: String = null): Unit ={
    val encodedQuery = encodeToBase64UTF8(nonEncodedQuery)
    val result = route(app, FakeRequest(GET, "/calculus?query=" + encodedQuery)).get
    status(result) mustBe Status.OK
    assertErrorJson(
      result, expectedMessage)
  }

  /** Assert whole request
    * Result has successfully evaluated value
    *
    * @param nonEncodedQuery
    * @param expectedNumber
    */
  def assertWholeRequest_Success(nonEncodedQuery: String, expectedNumber: BigDecimal): Unit = {
    val encodedQuery = encodeToBase64UTF8(nonEncodedQuery)
    val result = route(app, FakeRequest(GET, "/calculus?query=" + encodedQuery)).get
    // status OK
    status(result) mustBe Status.OK
    // json
    val parsedJsonContent: JsValue = assertJson(result)
    // "error"=false
    assertJsonField_error(parsedJsonContent, false)
    // "number"=<expectedNumber>
    assertJsonField_result(parsedJsonContent, expectedNumber)
  }

  def assertWholeRequest_Success(nonEncodedQuery: String, expectedNumber: Double): Unit = {
    assertWholeRequest_Success(nonEncodedQuery, BigDecimal(expectedNumber, getMathContext()))
  }

  def assertWholeRequest_Success(nonEncodedQuery: String, expectedNumber: Int): Unit = {
    assertWholeRequest_Success(nonEncodedQuery, BigDecimal(expectedNumber, getMathContext()))
  }

}
