package controllers

import java.util.Base64
import javax.inject._

import com.udojava.evalex.Expression
import play.api.libs.json.{Json, _}
import play.api.mvc._


object CalculatorController{
  final val precision = 12
  final val emptyInputMessage = "Empty input nothing to evaluate"
  final val illegalCharacterMessageBase = "Found illegal characters: "
  final val mismatchedParenthesesMessage = "Mismatched parentheses"
  final val allowedRuntimeExceptionMessages = Array(mismatchedParenthesesMessage)
  final val generalEvaluationErrorMessage = "Could not evaluate the expression"
  final val couldNotDecodeMessage = "Could not decode query parameter. (Base64 UTF-8 expected)"
}


class BadRequestError(s:String) extends Exception(s){}
class OkError(s:String) extends Exception(s){}


@Singleton
class CalculatorController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {



  /** Action
    * Decodes and evaluates math expression from the given query.
    * Allowed operators + - * / ( ) and numbers
    * Decimals are not allowed. (Can be changed by adding "." to the list and changing it's the test)
    *
    * 1) Decode decode Base64 UTF-8
    * 2) remove whitespaces
    * 3) Not empty
    * 4) Only valid chars
    * 5) Evaluate the expression
    * @param query Base64 UTF-8 encoded
    * @return
    *         If evaluation of the query was successful
    *         // SUCCESS
    *         OK json content {
    *           "error": false,
    *           "result": <evaluated decimal number>
    *         }
    *         // FAIL
    *         If could not evaluate the queyry
    *         OK json content {
    *           "error": true,
    *           "message": "<Why the evaluation failed, message can be shown to end user>"
    *         }
    *         If could not decode the query
    *         BAD_REQUEST json content {
    *           "error": false,
    *           "message": "<Why the error occurred, message for debugging>"
    *         }
    */
  def calculus(query: String) = Action {
    try{

      // 1) Decode decode Base64 UTF-8
      var decodedQuery: String = try {
        Base64.getDecoder.decode(query).map(_.toChar).mkString
      }catch{
        // FAIL-BadRequest - decoding
        case _: IllegalArgumentException =>
          throw new BadRequestError(CalculatorController.couldNotDecodeMessage)
      }
      // 2) remove whitespaces
      decodedQuery = decodedQuery.replaceAll("\\s", "")
      // 3) Not empty
      if(decodedQuery == ""){
        // FAIL-OK - empty query
        throw new OkError(CalculatorController.emptyInputMessage)
      }
      // 4) Only valid chars
      val illegalChars = hasIllegalChars(decodedQuery)
      if(illegalChars.nonEmpty){
        // FAIL-OK - illegal characters
        throw new OkError(
          CalculatorController.illegalCharacterMessageBase + illegalChars.take(10).mkString(", "))
      }

      // 5) Evaluate the expression
      val calculusResult: BigDecimal = try{
        evaluateCalculus(decodedQuery)
      }catch{
        // FAIL-OK
        case e: ArithmeticException => throw new OkError(e.getMessage)
        case e: RuntimeException => {
          // TODO support ExpressionException cases
          // Too general to allow messages go straight to user
          val message =
          if(CalculatorController.allowedRuntimeExceptionMessages.contains(e.getMessage)){
            e.getMessage
          }else{
            CalculatorController.generalEvaluationErrorMessage
          }
          throw new OkError(message)
        }
      }

      // SUCCESS
      createSuccessResult(calculusResult)
    }catch{
      case e: BadRequestError =>
        createBadRequestErrorResult(e.getMessage)
      case e: OkError =>
        createOkErrorResult(e.getMessage)
    }
  }


  /**
    * Returned when the error was seemingly made by client code NOT user.
    * These error messages should not be shown to user. That is why they are
    * wrapped in bad request. These can be useful in debugging.
    * e.g.
    * @param errorMessage
    * @return
    */
  def createBadRequestErrorResult(errorMessage: String): Result = {
    BadRequest(Json.obj(
      "error" -> JsBoolean(true),
      "message" -> JsString(errorMessage)
    ))
  }

  /**We were not able to evaluate the query.
    * Invalid input given by user.
    * These error messages can be shown to user.
    * @param errorMessage
    * @return
    */
  def createOkErrorResult(errorMessage: String): Result = {
    Ok(Json.obj(
      "error" -> JsBoolean(true),
      "message" -> JsString(errorMessage)
    ))
  }

  /**We were able to evaluate the query
    *
    * @param calculusResult
    * @return
    */
  def createSuccessResult(calculusResult: BigDecimal): Result = {
    Ok(Json.obj(
      "error" -> JsBoolean(false),
      "result" -> JsNumber(calculusResult)
    ))
  }

  def evaluateCalculus(decodedQuery: String): BigDecimal = {
    new Expression(decodedQuery).setPrecision(CalculatorController.precision).eval()
  }

  /**
    *
    * @param decodedQuery
    * @return
    */
  def hasIllegalChars(decodedQuery: String): List[Char] = {
    // Trimming done already but better to be sure
    val trimmedDecodedQuery: String = decodedQuery.replaceAll("\\s", "")
    val legalChars = Array(
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
      '+', '-', '*', '/', '(', ')'
    )
    val illegalChars = for(s <- trimmedDecodedQuery if !(legalChars contains s) ) yield s
    illegalChars.toList.distinct
  }

}
