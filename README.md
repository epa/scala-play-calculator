# scala-play-calculator

Small Scala Play demo project with one Json endpoint.

`/calculus/?query=<Base64 UTF-8 encoded mathematical expression>`
Returns evaluated result in as json response.

Example

- Non encoded query `2 * (23/(3*3))- 23 * (2*3)`
- Resulting URL `/calculus/?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=`
- Result (json response) `{"error":false,"result":-132.888888889}`

Live

- https://fast-forest-49909.herokuapp.com/
- https://fast-forest-49909.herokuapp.com/calculus/?query=MiAqICgyMy8oMyozKSktIDIzICogKDIqMyk=

